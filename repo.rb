# frozen_string_literal: true

require 'git'
require 'date'

class Repo

  attr_reader :git

  def initialize
    @git = Git.open(Pathname('team'))
    head
  end

  def deinit
    head
  end

  def checkout(sha)
    # puts "Checkout: #{sha}"
    return if sha.nil?
    git.checkout(sha.to_s)
  end

  def last_commit(year, month)
    before = Date.new(year, month).next_month.strftime
    git.log(1).until(before).first
  end

  private

  def head
    checkout 'master'
  end


end