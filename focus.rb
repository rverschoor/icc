# frozen_string_literal: true

class Focus

  attr_reader :names

  def initialize
    @names = {
      'L&R' => 'l&r',
      'License and Renewals' => 'l&r',
      'Management' => 'mgmt',
      'Onboarding' => 'train',
      'Onboaridng' => 'train',
      'Operations' => 'ops',
      'SaaS' => 'saas',
      'Saas' => 'saas',
      'SaaS Account' => 'acct',
      'Self Managed' => 'sm',
      'Self-Managed' => 'sm',
      'Unknown' => 'huh',
      'US Federal' => 'fed'
    }
  end

  def normalize(name)
    name ||= 'Unknown'
    if names[name].nil?
      puts "Unmapped focus: #{name}"
      exit 2
    end
    names[name]
  end

  def unique_names
    names.values.uniq.sort
  end

end
