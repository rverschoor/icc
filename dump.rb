# frozen_string_literal: true

require 'csv'

class Dump

  def save(focus, counts)
    # pp counts
    headers = %w(year month region nr ) + focus
    CSV.open('icc.csv', "wb", :headers => headers, :write_headers => true) do |csv|

      counts.each do |c|
        ff = []
        focus.each do |f|
          ff << c[:focus][f]
        end
        csv << [ c[:year], c[:month], c[:region], c[:nr] ] + ff
      end

    end
  end
end
  # {:year=>2023, :month=>4, :region=>"AMER-C", :nr=>13,
  # :focus=>{"ops"=>175, "mgmt"=>225, "sm"=>315, "fed"=>100, "l&r"=>225, "saas"=>260}}


