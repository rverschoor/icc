# IC Count

## NEEDS UPDATE

Team info moved to https://gitlab.com/gitlab-support-readiness/support-team and is now
structured differently.\
ICC needs to be adapated.

Clone https://gitlab.com/gitlab-com/support/team such that the `team` directory ends up as a subdir in this project.

```text
.
|____.gitignore
|____.ruby-version
|____Gemfile
|____Gemfile.lock
|____README.md
|____dump.rb
|____focus.rb
|____icc.csv
|____icc.rb
|____repo.rb
|____team
| |____(...)
| |____data
| | |____template.yaml
| | |____static_data.yaml
| | |____support-team.yaml
```

Run `bundle` to install required gem/s.

Run `icc.rb` to parse historic versions of `./team/data/support-team.yaml` and create `icc.csv`

