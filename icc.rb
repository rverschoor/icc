#!/usr/bin/env ruby
# frozen_string_literal: true

require 'yaml'
require 'pathname'
require_relative 'focus'
require_relative 'repo'
require_relative 'dump'

TEAM_YAML = './team/data/support-team.yaml'
START_YEAR = 2020

class Icc

  attr_reader :repo, :yaml, :focus, :counts, :dump

  def initialize
    @repo = Repo.new
    @counts = []
    @focus = Focus.new
    @dump = Dump.new
  end

  def process
    process_months
    save_counts
    repo.deinit
  end

  private

  def save_counts
    sort_counts
    dump.save focus.unique_names, counts
  end

  def sort_counts
    counts.sort_by! do |h|
      "#{"%4d%02d" % [h[:year], h[:month]]}"
    end
  end

  def process_months
    monthly_sha.each do |month|
      process_month month[:year], month[:month], month[:sha]
    end
  end

  def process_month(year, month, sha)
    puts "Process #{year}-#{month} #{sha}"
    repo.checkout sha
    process_yaml year, month
  end

  def process_yaml(year, month)
    @yaml = read_yaml
    return if yaml.nil?
    save_data count_data, year, month
  end

  def count_data
    data = {}
    yaml.each do |person|
      region = person['region']
      data[region] = data[region] || {}
      data[region]['nr'] = (data[region]['nr'] || 0) + 1
      data[region]['focus'] = data[region]['focus'] || {}
      person['focuses'] ||= [name: 'Unknown']
      person['focuses'].each do |f|
        name = focus.normalize f['name']
        perc = f['percentage']
        perc ||= 100
        begin
          data[region]['focus'][name] = (data[region]['focus'][name] || 0) + perc
        rescue => e
          puts e.message
          pp person
          puts "region: #{region}, name: #{name}, perc: #{perc}, focus: #{f}"
          exit 4
        end
      end
    end
    data
  end

  def save_data(data, year, month)
    data.each do |d|
      counts << { year: year, month: month, region: d[0], nr: d[1]['nr'], focus: d[1]['focus'] }
    end
  end

  def monthly_sha
    months = []
    this_year = Date.today.year
    this_month = Date.today.month
    (START_YEAR..this_year).reverse_each do |year|
      (1..12).each do |month|
        next if (year >= this_year) && (month > this_month)
        sha = repo.last_commit year, month
        unless sha.to_s.empty?
          months << { year: year, month: month, sha: sha.to_s }
        end
      end
    end
    months
  end

  def read_yaml
    return unless File.exist? TEAM_YAML
    YAML.load_file TEAM_YAML, permitted_classes: [Date]
  end

end

icc = Icc.new
icc.process
